$(document).ready(function () {

    initRotador();
    initContador();

    function initRotador() {
        //mezclar palabras
        $(".js-rotating").each(function () {
            var $this = $(this);
            var palabras = $this.html().split(",");
            shuffle(palabras);
            $this.html(palabras.join(","));
        });

        //inicializar rotador
        $(".js-rotating").Morphext({
            animation: "bounceIn",
            separator: ",",
            speed: 6000

        });
    }

    function initContador() {
        var fechaLanzamiento = new Date(2016, 3, 3);  //mes empieza en cero

        $('#counter').countdown({
            timestamp: fechaLanzamiento.getTime()
        });
    }


    function shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }

});
