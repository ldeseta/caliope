/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package club.caliope.controller;

import club.caliope.udc.InputFormat;
import club.caliope.udc.OutputFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

    @RequestMapping("/")
    public String home() {
        return "home";
    }

    @RequestMapping("/convertir")
    public String convertir(Model model) {
        model.addAttribute("mainInputFormats", mainInputFormats());
        model.addAttribute("mainOutputFormats", mainOutputFormats());
        model.addAttribute("allOutputFormats", allOutputFormats());
        return "convertir";
    }

    private List<InputFormat> mainInputFormats() {
        List<InputFormat> types = new ArrayList<>();
        types.add(InputFormat.DOCX);
        types.add(InputFormat.EPUB);
        types.add(InputFormat.HTML);
        types.add(InputFormat.ODT);
        return types;
    }

    private List<OutputFormat> mainOutputFormats() {
        List<OutputFormat> types = new ArrayList<>();
        types.add(OutputFormat.EPUB);
        types.add(OutputFormat.PDF);
        types.add(OutputFormat.HTML5);
        return types;
    }

    private List<OutputFormat> allOutputFormats() {
        List<OutputFormat> types = new ArrayList<>();
        types.add(OutputFormat.ASCIIDOC);
        types.add(OutputFormat.DOCX);
        types.add(OutputFormat.EPUB);
        types.add(OutputFormat.EPUB3);
        types.add(OutputFormat.HTML5);
        types.add(OutputFormat.JSON);
        types.add(OutputFormat.MARKDOWN);
        types.add(OutputFormat.OPENDOCUMENT);
        types.add(OutputFormat.PLAIN);
        types.add(OutputFormat.PDF);
        types.add(OutputFormat.RTF);
        return types;
    }
}
